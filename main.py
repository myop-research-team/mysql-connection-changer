from time import sleep
import subprocess
import requests
from os import path
from datetime import datetime

'''
Configuration settings for the code'''
config = {
    'connection_names' : ['connection1', 'connection2'],
    'connection1' : {
        'hostname' : 'localhost', 'username' : 'root', 'password' : '', 'backup_path' : '/tmp/connection1.php'
    },
    'connection2' : {
        'hostname' : 'localhost', 'username' : 'root', 'password' : '', 'backup_path' : '/tmp/connection2.php'
    },
    'dbconnection_path' : '/tmp/hello/dbconnection.php',
    'current_connection_path' : '/tmp/current_connection.log'
}


def check_paths():
    '''
    Check all files provided in the configuration exists'''
    global config

    for connection in config['connection_names']:
        if not path.isfile(config[connection]['backup_path']):
            print "Cannot find %s " %config[connection]['backup_path']
            exit()

    if not path.isfile(config['dbconnection_path']):
        print "Cannot find %s " %config['dbconnection_path']
        exit()

    if not path.isfile(config['current_connection_path']):
        print "Cannot find %s " %config['current_connection_path']
        exit()

def check_connection(hostname, username, password): 
    '''Check the connection by using mysql command

    **Inputs** : hostname, username, password //mysql connection details

    **Output** : **True** connection successful, **False** error in connection'''
    cmd = "timeout 10s mysql -h'%s' -u'%s' -p'%s' -e'show databases;'" %(hostname, username, password)
    proc =  subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    cmd_out = proc.stdout.read()
    if "information_schema" in cmd_out:
        return True
    else:
        return False

def change_connection(current_connection):
    '''
    Change the connection file of the database

    **Inputs** : name of the current connection
    
    **Steps** : 
    1. Get the new connection \n
    2. Write the new connection in the current connection path fot getting the current connection used in the next run \n
    3. Replace the db connection file with the new connection file \n '''
    global config
    connection_names = config['connection_names']
    if config['connection_names'][0] == current_connection:
        new_connection = config['connection_names'][1]
    elif config['connection_names'][1] == current_connection:
        new_connection = config['connection_names'][0]
    else:
        wl("Could not find new connection, something wrong with connection names")   
        exit() 

    f = open(config['current_connection_path'], "w")
    f.write(new_connection)
    f.close()

    wl(new_connection)

    cmd = "cp %s %s" %(config[new_connection]['backup_path'], config['dbconnection_path'])
    proc =  subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    cmd_out = proc.stdout.read()
    print cmd_out

def get_current_connection():
    '''Fetch the current connection from the new connection file
    
    **Inputs** : None 

    **Output** : connection name in the current connection file'''
    global config
    current_connection_path = config['current_connection_path']
    try:
        f = open(current_connection_path, "r")
        line = f.read().strip("\n").strip(" ")
        f.close()
        return line
    except Exception, e:
        print e.message

def wl(m):
    '''Logger method used for logging, prints the output to console

    **Inputs** : message (String)'''
    print( "%s : %s " %(str(datetime.now()), m) )

def send_alert():
    '''Send alert using elastalert server, while switching connection'''
    r = requests.post("http://52.76.62.6/alert_api.php", data={'type': 'Myop_app_db_changed', 'value': 1, 'desc': 'MyOperator '
        'application server has encountred database connection issue. Connection has been switched '})
    print(r.status_code, r.reason)

def process():
    '''Parent file, process starts here

    **Steps :**

    1. Check path of files provided in the config files \n
    2. Get current connection which is being used  \n
    3. Check DB connection, if success exit the script \n
    4. If Connection fails, recheck, if fails again, then change the connection \n

    '''
    wl("Cheking config settings")
    check_paths()

    wl("Fetching current connection")
    current_connection = get_current_connection()

    wl("Fetching current connection %s" %current_connection)
    wl("Check current connection")
    if check_connection(config[current_connection]['hostname'], config[current_connection]['username'], config[current_connection]['password']):
        wl("Connection Successful")
        exit()
    else:
        wl("Issue in connecting database, checking again...")
        sleep(1)
        if check_connection(config[current_connection]['hostname'], config[current_connection]['username'], config[current_connection]['password']):
            wl("Connection Successful")
            exit()
        else:
            wl("Not able to connect database after 2 attepts using connection %s" %current_connection)    
            wl("Sending alert using elastalert")
            send_alert()

            wl("Switching database connection...")
            change_connection(current_connection)
            wl("Connection changed")

if __name__ == "__main__":
    process();

