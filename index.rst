.. mysql connection changer documentation master file, created by
   sphinx-quickstart on Wed Nov  9 18:19:34 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mysql connection changer's documentation!
====================================================

Script to change the connection file of mysql with backup connection automatically.
Script will require 2 copies of database connection file with different connections, it will automatilcally switch the connection file if main connection is down.  

Contents:

.. toctree::
   :maxdepth: 2

   Tutorial
   About
   code
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

