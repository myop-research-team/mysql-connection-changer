Tutorial
========

Configuration
_____________

Configuration example is provided below:

config = {
    'connection_names' : ['connection1', 'connection2'],
    'connection1' : {
        'hostname' : 'localhost', 'username' : 'root', 'password' : '', 'backup_path' : '/tmp/connection1.php'
    },
    'connection2' : {
        'hostname' : 'localhost', 'username' : 'root', 'password' : '', 'backup_path' : '/tmp/connection2.php'
    },
    'dbconnection_path' : '/tmp/hello/dbconnection.php',
    'current_connection_path' : '/tmp/current_connection.log'
}

**connection_names** : name of the connections provided in the configuration
**connection** : connection settings (hostname , username, password, backup_path)
		backup_path is the path where connection file is stored
**dbconnection_poth** : path of the database file which is to be replaced
**current_connection_path** : this file will store the connection which is currently active

